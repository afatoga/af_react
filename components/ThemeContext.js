import { createContext } from 'react';

const ThemeContext = createContext();

export default ThemeContext;

/*class ThemeContextProvider extends Component {
    state = { 
        lang: "cs",
        links : [
            { href: 'https://zeit.co/now', label: 'ZEIT' },
            { href: 'https://github.com/zeit/next.js', label: 'GitHub' }
        ]
    }
    render() { 
        return (  
            <ThemeContext.Provider value={{...this.state}}>
                {this.props.children}
            </ThemeContext.Provider>
        );
    }
}
 
export default ThemeContextProvider;*/