import React from "react";
import styled from "styled-components";
import Carousel from 'react-bootstrap/Carousel';

const PrevIcon = () => (
  <img src='/LS_ikona_sipka_doleva.png'></img>
)

const NextIcon = () => (
  <img src='/LS_ikona_sipka_doprava.png'></img>
)

const ReviewCarousel = props => {
    if (props.reviews.length) {
    
        let controls = (props.reviews.length == 1) ? false : true;

        return (
            <>  
                <div className="row">
                    <CarouselContainer className="col-lg-12">
                    <Carousel indicators={false} controls={controls} prevIcon={<PrevIcon />} nextIcon={<NextIcon/>}>
                        {props.reviews.map((item, i) => (
                            <Carousel.Item className="p-4" key={i}>
                                <Container>
                                  <AF_ReviewText dangerouslySetInnerHTML={{__html: item.content}} />
                                  <AF_ReviewAuthor>
                                      <span className="text-uppercase font-weight-bold">{item.title}</span>, <AF_ReviewAuthorCity dangerouslySetInnerHTML={{__html: item.meta["af_reviewerLocation"][0]}} />
                                  </AF_ReviewAuthor>
                                </Container>
                            </Carousel.Item>
                        ))}
                    </Carousel>
                    </CarouselContainer>
                </div>
            </> 
         );
    
    }
};

export default ReviewCarousel;

const CarouselContainer = styled.div`
`

const Container = styled.div`
  min-height: 10rem;
  vertical-align: middle;
`;

const AF_ReviewText = styled.p`
  text-align: left;
  width: 80%;
  margin: 0 auto;
  font-style: italic;
  line-height: 190%;
`;

const AF_ReviewAuthor = styled.p`
  text-align: right;
  width: 80%;
  font-size: 1rem;
`;

const AF_ReviewAuthorCity = styled.span`
  font-size: 0.8rem;
  font-style: italic;
`;