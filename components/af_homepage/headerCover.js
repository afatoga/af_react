import React from "react";
import styled from "styled-components";

const HeaderCover = props => {
  return (
    <>
            <div className="row no-gutters">
                <div className="col-12">
                    <Container className="af_homepageCover">
                    <span className="af_mainHeaderQuote" dangerouslySetInnerHTML={{__html: props.homepage[3].content}} />
                    <p className="af_buttomHeaderQuote" dangerouslySetInnerHTML={{__html: props.homepage[5].meta["af_headerCoverQuote"][0]}} />
                    </Container>
                </div>
            </div>

            <style global jsx>{`
              span.af_mainHeaderQuote {
                font-weight:500;
                font-size: 3rem;
                line-height: 3.2rem;
              }

              p.af_buttomHeaderQuote {
                margin-top: 1rem;
                font-weight: 500;
                font-size: 1.8rem;
                line-height: 2rem;
              }

              @media (min-width: 1200px) {
                  .navbar-light .navbar-nav .nav-link {
                    padding: 0 1rem;
                  }
              }
            `}</style>
    </> 
 );
};

const Container = styled.div`
  background-image: url(https://ipsumimage.appspot.com/1100x440?f=000&s=0);
  text-align: center;
  height: 350px;
  padding-top:3rem;
  color: #fff;
`;

export default HeaderCover;