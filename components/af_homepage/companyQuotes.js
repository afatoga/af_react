import React from "react";
import styled from "styled-components";

const CompanyQuotes = props => {
  return (
    <>
            <div className="row">
                <div className="col-lg-6 px-4 text-center sm-md-hidden">
                    <ColoredLine />
                        <p className="companyQuote" dangerouslySetInnerHTML={{__html: props.homepage[5].meta["af_quote01"][0]}} />
                    <ColoredLine />
                </div>
                <div className="col-lg-6 px-4 text-center sm-md-hidden">
                    <ColoredLine />
                        <p className="companyQuote" dangerouslySetInnerHTML={{__html: props.homepage[5].meta["af_quote02"][0]}} />
                    <ColoredLine />
                </div>
            </div>

            <style global jsx>{`
              p.companyQuote {
                font-weight:700;
              }

              @media (min-width: 1200px) {
                  .navbar-light .navbar-nav .nav-link {
                    padding: 0 1rem;
                  }
              }
            `}</style>
    </> 
 );
};

const ColoredLine = styled.hr`
    border: 0.05rem solid gray;
`;

export default CompanyQuotes;