import React from "react";
import styled from "styled-components";

const PageContent = props => {
  return (
    <>
                <div className="col-12 pb-2">
                    <Container className="af_pageContent pb-4">
                    <h2 className="text-uppercase">Chci objednat kurz pro firmu</h2>
                    <p>- text</p>
                    <p>- dalsi text</p>
                    <button type="button" className="af_orderCourseButton btn btn-danger">objednavkový formulář pro firmy</button>
                    </Container>
                </div>

            <style global jsx>{`
            h2.text-uppercase {
                color: #bf0d0d;
            }

            button.af_orderCourseButton {
                padding:0.4rem 0.6rem;
                border-radius: 0;
                background-color: #bf0d0d;
            }
            `}</style>
    </> 
 );
};

const Container = styled.div`
  text-align: left;
  font-size: 1rem;
`;

export default PageContent;