import Head from 'next/head';
import NavigationMenu from './navigationMenu';
import styled from 'styled-components';

const Layout = props => {
  return (
    <>
      <Head>
        <title>Lifesupport</title>
        <meta name="description" content="Lifesupport.cz react aplikace" />
        <link
            rel="stylesheet"
            href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
            integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
            crossOrigin="anonymous"
        />
        <link 
          href="https://fonts.googleapis.com/css?family=Fira+Sans:400,400i,500,700&display=swap&subset=latin-ext" 
          rel="stylesheet">
        </link>
        <link rel='icon' href='/favicon.ico' />
      </Head>


      <div className="container">
        <NavigationMenu>{props.menu}</NavigationMenu>
        <div className="row main-content">
          <MainContent className="col-12">
            {props.children}
          </MainContent>
        </div>
      </div>

        <style jsx global>{`
        * { 
        box-sizing: border-box; 
        margin: 0; 
        padding: 0 
        }
        a, a:visited {
          color: inherit;
          text-decoration: none;
        }
        body { 
          font-family: 'Fira Sans', Segoe UI, sans-serif;
        }

        .main-content {
          padding-bottom:4rem;
        }

        @media (max-width: 991px) {
          .main-content {
            position: relative;
          }
          
          #basic-navbar-nav {
            position: absolute;
            top: 3.2rem;
            z-index: 1;
          }

          .sm-md-hidden {
            display: none;
          }
        }
      `}</style>
    </>
  );
};

const MainContent = styled.div`
`

export default Layout;
