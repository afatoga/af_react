import { useContext } from 'react'
import Link from 'next/link'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import ThemeContext from '../components/ThemeContext'
import styled from 'styled-components';

const NavigationMenu = props => {

  const { lang, switchLang } = useContext(ThemeContext);
  const menu = props.children;
  let route = "/";

  if (lang == "en") {
    route = "/en/";
  }

  return (
  <>
    <div className="row">
      <LanguageSwitch className="col-lg-12">
        <button className="btn" onClick={switchLang}>
          Switch
        </button>
      </LanguageSwitch>
    </div>
    <div className="row d-lg-none d-xl-none">
      <div className="col-md-12 sm-md-logo">
        <img src="/LS_logo.png" alt="my image" width="160px" className="sm-md-logo"/>
      </div>
    </div>
    <div className="row" id="af_navbarRow">
      <div className="col-lg-12">
        <Navbar bg="white" variant="light" expand="lg">
        <Navbar.Brand href="/" className="sm-md-hidden">
          <img
            src="/LS_logo.png"
            width="160"
            className="align-bottom"
            alt="React Bootstrap logo"
          />
        </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ml-auto">
              {menu.map((item, i) => (
                  <Nav.Link key={i} href={route+item.url[1]}>{item.title}</Nav.Link>
                ))}
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </div>
    </div>

    <style global jsx>{`
        .navbar {
          font-size: 1.2rem;
          font-weight: bold;
          text-transform: uppercase;
        }
        .navbar-light .navbar-nav .nav-link {
          color: #000;
          padding-left: 0.3rem;
          padding-right: 0.3rem;
        }
        .navbar-light .navbar-nav .nav-link:hover {
          color: #bf0d0d;
        }

        .navbar-light .navbar-toggler-icon {
          background-image: url("/menu.png");
          border-color: #fff;
          width: 2.4em;
          height: 1.6em;
        }

        .navbar-light .navbar-toggler {
          border: none;
        }

        @media (max-width: 991px) {
          .navbar-nav {
            padding-left: 0.9rem;
          }

          .navbar, .bg-white {
            background-color: transparent !important;
          }

          div#af_navbarRow {
            position:absolute;
            top: 6rem;
            z-index: 1;
          }

          div.sm-md-logo {
            text-align: center;
            background-color: #bf0d0d;
          }

          .navbar-light .navbar-nav .nav-link {
            background-color: #fff;
            padding: 0.4rem 1rem;
            margin-bottom: 1rem;
            letter-spacing: 0.2rem
          }

          .navbar-light .navbar-nav .nav-link:active {
            background-color: #bf0d0d;
            color: #fff;
          }
        }

        @media (min-width: 992px) {
          .navbar {
            padding: 0;
          }
        }

        @media (min-width: 1200px) {
          .navbar-light .navbar-nav .nav-link {
            padding: 0 1rem;
          }
        }
      `}</style>
  </>
  )
}

const LanguageSwitch = styled.div`
color: #000;
text-align: right;
`


export default NavigationMenu
