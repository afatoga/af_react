import { useRouter } from 'next/router';

const Page = () => {
  const router = useRouter();
  const { page } = router.query;

  return (<p>PageCs: {page}</p>)
}

export default Page;