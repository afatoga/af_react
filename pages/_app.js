import React from 'react';
import App from 'next/app';
import Router from 'next/router';
import { ThemeProvider } from 'styled-components';
import ThemeContext from '../components/ThemeContext';

const theme = {
  colors: {
    primary: '#0070f3'
  }
}

export default class MyApp extends App {
  state = {
    lang: "cs"
  };
  //urcit state dle routeru!!
  
  switchLang = () => {
    if (this.state.lang == "cs") 
    {
      this.setState({
        lang: "en"
      });
      Router.push('/en'); 
    } else {
      this.setState({
        lang: "cs"
      });
      Router.push('/');
    }
    
  };

  setLang = (lang) => {
    if (this.state.lang == "cs") 
    {this.setState({
      lang: lang
    });    }
    
  };

  render () {
    const { Component, pageProps } = this.props
    return (
      <ThemeContext.Provider value={{ lang: this.state.lang, switchLang: this.switchLang, setLang: this.setLang}}>
        <ThemeProvider theme={theme}>
            <Component {...pageProps} />
        </ThemeProvider>
      </ThemeContext.Provider>
    )
  }
}