import React from 'react';
import Error from 'next/error';
import Layout from '../components/layout';
import HeaderCover from '../components/af_homepage/headerCover';
import CompanyQuotes from '../components/af_homepage/companyQuotes';
import ReviewCarousel from '../components/af_homepage/reviewCarousel';
import PageContent from '../components/af_homepage/pageContent';
import styled from 'styled-components';
import fetch from 'isomorphic-unfetch';

const Home = props => {
  if (props.errorCode) 
  {
    return <Error statusCode={errorCode} />
  }
  return (
      <Layout menu={props.menu}>
          <HeaderCover homepage={props.homepage} />
          <CompanyQuotes homepage={props.homepage} />
          <ReviewCarousel reviews={props.reviews} />
          <div className="row pt-2">
            <div className="col-8">
              <PageContent />
              <PageContent />
            </div>
            <div className="col-4 af_courseOfferTab">
              <p className="af_publicCoursesHeading">Nejbližší kurzy pro&nbsp;veřejnost</p>
                <div className="row af_courseItem pb-2">
                  <div className="col-9">
                    <p className="af_courseDate">20. 8. 2018</p>
                    <p className="af_courseName">Kurz resuscitace</p>
                    <p className="af_courseLocation text-uppercase">Liberec</p>
                  </div>
                  <div className="col-3">
                    <img src="/LS_ikona_kosik_kurzy.png"></img>
                    <p className="af_coursePrice pt-2">2&nbsp;000&nbsp;Kč</p>
                  </div>
                </div>
                <div className="row af_courseItem pb-2">
                  <div className="col-9">
                    <p className="af_courseDate">20. 8. 2018</p>
                    <p className="af_courseName">Kurz resuscitace</p>
                    <p className="af_courseLocation text-uppercase">Liberec</p>
                  </div>
                  <div className="col-3">
                    <img src="/LS_ikona_kosik_kurzy.png"></img>
                    <p className="af_coursePrice pt-2">2&nbsp;000&nbsp;Kč</p>
                  </div>
                </div>
                <div className="row af_courseItem pb-2">
                  <div className="col-9">
                    <p className="af_courseDate">20. 8. 2018</p>
                    <p className="af_courseName">Kurz resuscitace</p>
                    <p className="af_courseLocation text-uppercase">Liberec</p>
                  </div>
                  <div className="col-3">
                    <img src="/LS_ikona_kosik_kurzy.png"></img>
                    <p className="af_coursePrice pt-2">2&nbsp;000&nbsp;Kč</p>
                  </div>
                </div>
              <p className="af_publicCoursesFooter">Zobrazit všechny kurzy</p>
            </div>
          </div>

          <div className="row pt-4 af_instructorsBlock">
            <div className="col-12">
              <h2 className="text-uppercase">Kurzy vás provedou</h2>
              <div className="af_carouselInstructors">Tváře</div>
            </div>
          </div>

          <div className="af_teamBuildingBlock row pt-4">
            <div className="col-12 af_teamBuilding">
              <h3 className="text-uppercase">TeamBuilding</h3>
              <div className="row">
                  <div className="col-7">
                    <p>text text</p>
                  </div>
                  <div className="col-5">
                    <p> video</p>
                  </div>
              </div>
          </div>
          </div>

          <div className="af_instructorsBlock row pt-4">
              <div className="col-12 af_teamBuilding">
                <h4 className="text-uppercase">Blog - Jan Bradna</h4>
                <p>Krystov - pomoc z nebe</p>
                <p>Dlouhy text...</p>
              </div>
          </div>

          <div className="row pt-4 af_instructorsBlock">
            <div className="col-12">
              <h2 className="text-uppercase">Videa, která se vám mohou hodit už dnes</h2>
              <div className="af_carouselInstructors">Videa</div>
            </div>
          </div>

          <div className="footer row pt-5">
            <div className="col-4">
              <img src="/LS_ikona_socialni_site.png"></img>
            </div>
            <div className="col-2 text-left">
              <p>odkaz</p>
              <p>odkaz</p>
              <p>odkaz</p>
              <p>odkaz</p>
            </div>
            <div className="col-2 text-left">
              kontakt
            </div>
            <div className="col-4">
            <img src="/LS_ikona_mapa.png"></img>
            </div>
          </div>

          <style global jsx>{`
              div.af_instructorsBlock {
                padding-left: 22.5px;
              }

              div.af_teamBuildingBlock {
                padding-left: 20px;
              }

              div.af_teamBuilding {
                border-bottom: 1px solid #ccc;
              }

              h3.text-uppercase {
                font-size:2rem;
                font-weight:700;
              }

              .af_courseOfferTab {
                border-left: 0.1rem solid #ccc;
                padding-left: 2rem;
              }

              .af_publicCoursesHeading {
                color: #cfcfcf;
                padding:0 0 1rem;
                margin:0;
              }

              .af_courseDate {
                font-weight:700;
                font-size:1.5rem;
                line-height: 1.5rem;
                margin:0;
                padding:0;
              }
              
              .af_courseName {
                color:#bf0d0d;
                font-weight:700;
                font-size: 1.1rem;
                padding: 0.2rem 0 0;
                margin: 0;
              }

              .af_publicCoursesFooter {
                color:#bf0d0d;
                font-weight:700;
                font-size: 1.1rem;
              }

              .af_carouselInstructors {
                background-image: url(https://ipsumimage.appspot.com/1100x440?f=000&s=0);
                text-align: center;
                height: 100px;
                padding-top:3rem;
              }
          `}</style>
      </Layout>
  )
}


/*const Title = styled.h1`
  font-size: 50px;
  color: ${({ theme }) => theme.colors.primary};
`
*/

Home.getInitialProps = async function() {
  let res = await fetch('http://224489.w89.wedos.ws/wp-json/menus/v1/menus/headerMenu-cs');
  let menuCs = await res.json();

  let config = {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
    //body: JSON.stringify({
  };
  res = await fetch('http://224489.w89.wedos.ws/wp-json/af_restServer/v1/af_post_by_slug?slug=homepage&post_type=page', config);
  let homepage = await res.json();

  res = await fetch('http://224489.w89.wedos.ws/wp-json/af_restServer/v1/af_posts_by_cat?cat=recenze', config);
  let reviews = await res.json();

  const errorCode = res.statusCode > 200 ? res.statusCode : false

  return {
    errorCode,
    menu: menuCs.items.map(item => ({
      title: item.title,
      url: /http:\/\/.*\/(.+)\//.exec(item.url)})),
    homepage: homepage,
    reviews: reviews
  };
};
//{menuCs: props.menuCs, menuEn: props.menuEn}
export default Home;