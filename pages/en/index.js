import React, { useContext } from 'react';
import Layout from '../../components/layout';
import styled from 'styled-components';
import fetch from 'isomorphic-unfetch';
import ThemeContext from '../../components/ThemeContext';

const HomeEn = props => {
    //const { lang, setLang } = useContext(ThemeContext);
    //setLang("en");
    //console.log(lang)

    return (
        <Layout menu={props.menu}>
            <Title>Lifesupport En</Title>
        </Layout>
    )
}

const Title = styled.h1`
  font-size: 50px;
  color: ${({ theme }) => theme.colors.primary};
  font-family: 'Fira Sans', sans-serif;
`

HomeEn.getInitialProps = async function() {

  let res = await fetch('http://224489.w89.wedos.ws/wp-json/menus/v1/menus/headerMenu-en');
  let menuEn = await res.json();

  return {
    menu: menuEn.items.map(item => ({
      title: item.title,
      url: /http:\/\/.*\/(.+)\//.exec(item.url)}))
  };
};

export default HomeEn;