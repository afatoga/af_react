import { useRouter } from 'next/router';

const PageEn = () => {
  const router = useRouter();
  const { page } = router.query;
  return (<p>PageEn: {page}</p>)
}

export default PageEn;